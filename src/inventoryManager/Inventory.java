/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

/**
 * Inventory container class.
 *
 * @author Eric Boring
 */
public class Inventory {

    private ProductList<Product> products = new ProductList<>();
    private PartsList<Part> allParts = new PartsList<>();

    /**
     * Adds a product to the inventory.
     *
     * @param p The product to add.
     */
    public void addProduct(Product p) {
        products.add(p);
    }

    /**
     * Removes a product from the inventory.
     *
     * @param p The product to remove
     * @return A boolean indicating whether the removal was successful.
     */
    public boolean removeProduct(Product p) {
        return products.remove(p);
    }

    /**
     * Removes a product from the inventory by the product's ID.
     *
     * @param id The ID of the product to remove
     * @return A boolean indicating whether the removal was successful.
     */
    public boolean removeProduct(int id) {
        return removeProduct(lookupProduct(id));
    }

    /**
     * Returns the product identified by the ID number.
     *
     * @param id
     * @return
     */
    public Product lookupProduct(int id) {
        return products.getById(id);
    }

    /**
     * Updates a product.
     *
     * @param id
     * @deprecated
     */
    public void updateProduct(int id) {
        //Products are updated directly in the data stucture by the
        //EditProductController and the TableView updates automatically due to
        //the ObservableList interface and accompanying built in JavaFX event
        //notifiers.
    }

    /**
     * Adds a part to the inventory.
     *
     * @param p The part to add.
     */
    public void addPart(Part p) {
        allParts.add(p);
    }

    /**
     * Deletes a part from the inventory.
     *
     * @param p The part to delete.
     * @return A boolean indicating whether the removal was successful.
     */
    public boolean deletePart(Part p) {
        return allParts.remove(p);
    }

    /**
     * Find a part by it's ID number.
     *
     * @param id The ID number of the part to find.
     * @return The part identified by the ID number.
     */
    public Part lookupPart(int id) {
        return allParts.getById(id);
    }

    /**
     * Updates a part.
     *
     * @param id
     * @deprecated
     */
    public void updatePart(int id) {
        //Parts are updated directly in the data stucture by the
        //EditPartController and the TableView updates automatically due to the
        //ObservableList interface and accompanying built in JavaFX event
        //notifiers.
    }

    /**
     * Returns the list of all parts this inventory contains.
     *
     * @return A PartsList of all parts in this inventory.
     */
    public PartsList<Part> getAllParts() {
        return allParts;
    }

    /**
     * Sets a new PartsList as the parts list for this inventory.
     *
     * @param allParts The new parts list.
     */
    public void setAllParts(PartsList<Part> allParts) {
        this.allParts = allParts;
    }

    /**
     * Returns the list of all products this inventory contains.
     *
     * @return A ProductList of all the products in this inventory.
     */
    public ProductList<Product> getAllProducts() {
        return products;
    }

    /**
     * Sets a new ProductList as the product list for this inventory.
     *
     * @param allProducts The new product list.
     */
    public void setAllProducts(ProductList<Product> allProducts) {
        this.products = allProducts;
    }
}
