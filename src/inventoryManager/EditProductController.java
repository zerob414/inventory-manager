/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class Controller class for the Edit Product dialog.
 *
 * @author Eric Boring <eboring@wgu.edu>
 */
public class EditProductController implements Initializable {

    private Stage myStage;
    private MainWindowController parent;
    private Product product;

    @FXML
    private TextField txtId;
    @FXML
    private TextField txtName;
    @FXML
    private Spinner spnInvLevel;
    @FXML
    private TextField txtPrice;
    @FXML
    private Spinner spnMin;
    @FXML
    private Spinner spnMax;
    @FXML
    private TextField txtSearch;
    @FXML
    private Hyperlink lblPrice;
    @FXML
    private TableView tblPartInventory;
    @FXML
    private TableColumn colInvId;
    @FXML
    private TableColumn colInvName;
    @FXML
    private TableColumn colInvStock;
    @FXML
    private TableColumn colInvCost;
    @FXML
    private TableView tblProductParts;
    @FXML
    private TableColumn colProdId;
    @FXML
    private TableColumn colProdName;
    @FXML
    private TableColumn colProdStock;
    @FXML
    private TableColumn colProdCost;

    /**
     * Constructs this EditProductController.
     */
    public EditProductController() {
    }

    /**
     * Constructs this EditProductController and sets it's stage.
     *
     * @param myStage
     */
    public EditProductController(Stage myStage) {
        this.myStage = myStage;
    }

    /**
     * Constructs this EditProductController then sets it's stage and parent.
     *
     * @param myStage
     * @param parent
     */
    public EditProductController(Stage myStage, MainWindowController parent) {
        this.myStage = myStage;
        this.parent = parent;
    }

    /**
     * Constructs this EditProductController then sets it's stage, parent and
     * initial Product object.
     *
     * @param myStage
     * @param parent
     * @param product
     */
    public EditProductController(Stage myStage, MainWindowController parent, Product product) {
        this.myStage = myStage;
        this.parent = parent;
        this.setProduct(product);
    }

    /**
     * Handler for when the user presses the "Search" button for parts. Filters
     * based on the text entered in the search text field. The user may also
     * enter a regular expression for advanced searching.
     */
    @FXML
    public void actionSearchParts() {
        PartsList filtered = parent.getInventory().getAllParts().filter(txtSearch.getText().trim());
        tblPartInventory.setItems(filtered);
    }

    /**
     * Handler for when the user presses the "X" button for parts. Clears the
     * search text field and displays all parts in the table.
     */
    @FXML
    public void actionClearPartSearch() {
        txtSearch.setText("");
        actionSearchParts();
    }

    /**
     * Handler for when the user presses the "Add Part" button. Adds the
     * selected part to the PartsList for the current product.
     */
    @FXML
    public void actionAddPart() {
        Object selObj = tblPartInventory.getSelectionModel().getSelectedItem();
        if (selObj != null && selObj instanceof Part) {
            tblProductParts.getItems().add(selObj);
        }
        updatePrices();
    }

    /**
     * Handler for when the user presses the "Remove Part" button. Removes the
     * selected part from the PartsList for the current product.
     */
    @FXML
    public void actionRemovePart() {
        Object selObj = tblProductParts.getSelectionModel().getSelectedItem();
        if (selObj != null && selObj instanceof Part) {
            tblProductParts.getItems().remove(selObj);
        }
        updatePrices();
    }

    /**
     * Handler for when the user presses the "Save" button. If the controller's
     * product is null, a new Product object will be created and added to the
     * inventory, otherwise the product will be modified.
     *
     * @param e
     */
    @FXML
    public void actionSave(ActionEvent e) {
        if (validateInput()) {
            if (this.product == null) {
                this.product = new Product(Integer.parseInt(txtId.getText()), txtName.getText(), Double.parseDouble(txtPrice.getText()), (Integer) spnInvLevel.getValue(), (Integer) spnMin.getValue(), (Integer) spnMax.getValue(), (PartsList) tblProductParts.getItems());
                parent.getInventory().addProduct(this.product);
            } else {
                this.product.setAll(Integer.parseInt(txtId.getText()), txtName.getText(), Double.parseDouble(txtPrice.getText()), (Integer) spnInvLevel.getValue(), (Integer) spnMin.getValue(), (Integer) spnMax.getValue(), (PartsList) tblProductParts.getItems());
                parent.refreshTables();
            }
            myStage.hide();
            clearForm();
        }
    }

    /**
     * Handler for when the user presses the "Cancel" button. A confirmation
     * dialog box is displayed.
     *
     * @param e
     */
    @FXML
    public void actionCancel(ActionEvent e) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Inventory Management System - Product Editor");
        alert.setHeaderText("Do you want to discard your changes to this product?");
        //alert.setContentText("Choose your option.");

        ButtonType btnYes = new ButtonType("Yes");
        ButtonType btnNo = new ButtonType("No");

        alert.getButtonTypes().setAll(btnYes, btnNo);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(parent.getStage());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btnYes) {
            myStage.hide();
            clearForm();
        }
    }

    /**
     * When the user clicks on the suggested price for the product, this method
     * puts that suggested price in the Price text field.
     */
    @FXML
    public void actionSetSuggestedPrice() {
        txtPrice.setText(Double.toString(Product.getSuggestedPrice(tblProductParts.getItems())));
    }

    /**
     * Updates the label that shows the total cost of the parts added to this
     * Product as well as the suggested price based on a 25% markup.
     */
    public void updatePrices() {
        lblPrice.setText("Total Parts Cost: $" + PartsList.getTotalCost(tblProductParts.getItems()) + "\nSuggested Price: $" + Product.getSuggestedPrice(tblProductParts.getItems()));
    }

    /**
     * Validates all data entered by the user and displays an error dialog if
     * any data is invalid.
     *
     * @return Boolean value indicating whether validation succeeded.
     */
    private boolean validateInput() {
        boolean valid = true;
        String errorMessage = "";
        if (!Regex.match("^\\d+$", txtId.getText().trim())) {
            errorMessage += "\u2022 ID can only be digits.\n";
            valid = false;
        }
        if (txtName.getText().length() < 4) {
            errorMessage += "\u2022 Name must be descriptive.\n";
            valid = false;
        }
        if (txtName.getText().length() > 255) {
            errorMessage += "\u2022 Name cannot be longer than 255 characters.\n";
            valid = false;
        }
        if (Regex.match("^\\d+$", txtName.getText().trim())) {
            errorMessage += "\u2022 Name cannot be only digits.\n";
            valid = false;
        }
        if ((int) spnInvLevel.getValue() < (int) spnMin.getValue() || (int) spnInvLevel.getValue() > (int) spnMax.getValue()) {
            errorMessage += "\u2022 Inventory level must be between minimum and maximum inventory.\n";
            valid = false;
        }
        if (!Regex.match("^\\d+\\.?\\d*$", txtPrice.getText().trim()) || txtPrice.getText().trim().length() == 0) {
            errorMessage += "\u2022 Cost must be a valid positive number.\n";
            valid = false;
        } else if (Double.parseDouble(txtPrice.getText()) < PartsList.getTotalCost(tblProductParts.getItems())) {
            errorMessage += "\u2022 Cost must be more than the total cost of all the parts in this product.\n";
            valid = false;
        }
        if ((int) spnMin.getValue() > (int) spnMax.getValue()) {
            errorMessage += "\u2022 Minimum inventory must be less or equal to maximum inventory.\n";
            valid = false;
        }
        if ((int) spnMin.getValue() < 0) {
            errorMessage += "\u2022 Minimum inventory must be greater than zero.\n";
            valid = false;
        }
        if ((int) spnMax.getValue() < 0) {
            errorMessage += "\u2022 Maximum inventory must be greater than zero.\n";
            valid = false;
        }
        if (tblProductParts.getItems().isEmpty()) {
            errorMessage += "\u2022 This product must have at least one part.\n";
            valid = false;
        }

        if (!valid) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Invalid Values Entered");
            alert.setHeaderText("You have entered some invalid values, please fix the issues below to continue.");
            alert.setContentText(errorMessage);
            alert.showAndWait();
        }
        return valid;
    }

    /**
     * Binds the table columns to the proper fields of the Part objects and
     * initializes the spinners.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        colInvId.setCellValueFactory(new PropertyValueFactory<Part, Integer>("partId"));
        colInvName.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        colInvStock.setCellValueFactory(new PropertyValueFactory<Part, Integer>("level"));
        colInvCost.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));

        colProdId.setCellValueFactory(new PropertyValueFactory<Part, Integer>("partId"));
        colProdName.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        colProdStock.setCellValueFactory(new PropertyValueFactory<Part, Integer>("level"));
        colProdCost.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));

        SpinnerValueFactory<Integer> svfInvLevel = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 1);
        spnInvLevel.setValueFactory(svfInvLevel);

        SpinnerValueFactory<Integer> svfMin = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 1);
        spnMin.setValueFactory(svfMin);

        SpinnerValueFactory<Integer> svfMax = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 1);
        spnMax.setValueFactory(svfMax);

        /*
        In JavaFX, spinners do not commit typed in values to the ValueFactory
        until the user presses the "Enter" key. This is apparently deliberate
        behavior according to the documentation, although it goes against 
        expected behavior and is frustrating. The following code to solve this
        problem is borrowed from Sergio and Robert here:
        https://stackoverflow.com/questions/32340476/manually-typing-in-text-in-javafx-spinner-is-not-updating-the-value-unless-user
         */
        for (Field field : getClass().getDeclaredFields()) {
            try {
                Object obj = field.get(this);
                if (obj != null && obj instanceof Spinner) {
                    ((Spinner) obj).focusedProperty().addListener((observable, oldValue, newValue) -> {
                        if (!newValue) {
                            ((Spinner) obj).increment(0);
                        }
                    });
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sets this controller's stage.
     *
     * @return
     */
    public Stage getStage() {
        return myStage;
    }

    /**
     * Returns this controller's stage.
     *
     * @param stage
     */
    public void setStage(Stage stage) {
        this.myStage = stage;
    }

    /**
     * Returns this controller's parent controller.
     *
     * @return
     */
    public MainWindowController getParent() {
        return parent;
    }

    /**
     * Sets this controller's parent controller.
     *
     * @param parent
     */
    public void setParent(MainWindowController parent) {
        this.parent = parent;
    }

    /**
     * Returns the product this controller is currently viewing/modifying.
     *
     * @return
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets the product that this controller is currently viewing/modifying.
     * Additionally this method will populate all of the form's fields with the
     * Product's data or will clear the form if null is passed.
     *
     * @param product
     */
    public void setProduct(Product product) {
        this.product = product;
        if (product == null) {
            clearForm();
        } else {
            txtId.setText(Integer.toString(product.getProductId()));
            txtName.setText(product.getName());
            spnInvLevel.getValueFactory().setValue(product.getInStock());
            txtPrice.setText(Double.toString(product.getPrice()));
            spnMax.getValueFactory().setValue(product.getMaximum());
            spnMin.getValueFactory().setValue(product.getMinimum());
            tblProductParts.setItems(product.getAssociatedParts());
            updatePrices();
        }
        actionClearPartSearch();
    }

    /**
     * Clears the form's fields and sets them to default values.
     */
    public void clearForm() {
        txtId.setText(Integer.toString(parent.getInventory().getAllProducts().getNewId()));
        txtName.setText("");
        spnInvLevel.getValueFactory().setValue(0);
        txtPrice.setText("0.00");
        spnMax.getValueFactory().setValue(10);
        spnMin.getValueFactory().setValue(0);
        tblProductParts.setItems(new PartsList());
        updatePrices();
    }
}
