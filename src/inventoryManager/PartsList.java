/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Custom list class to provide some extra helper/convenience methods for lists
 * of Part objects.
 *
 * @author Eric Boring
 * @param <E>
 */
public class PartsList<E> extends SimpleListProperty<E> {

    /**
     * Constructs a new PartsList with an empty observableArrayList.
     */
    public PartsList() {
        super(FXCollections.observableArrayList());
    }

    /**
     * Constructs a new PartsList with a supplied observableArrayList.
     *
     * @param initialValue
     */
    public PartsList(ObservableList<E> initialValue) {
        super(initialValue);
    }

    /**
     * Constructs a new PartsList with an empty observableArrayList and supplied
     * bean and name.
     *
     * @param bean
     * @param name
     */
    public PartsList(Object bean, String name) {
        super(bean, name, FXCollections.observableArrayList());
    }

    /**
     * Constructs a new PartsList with a supplied observableArrayList bean and
     * name.
     *
     * @param bean
     * @param name
     * @param initialValue
     */
    public PartsList(Object bean, String name, ObservableList<E> initialValue) {
        super(bean, name, initialValue);
    }

    /**
     * Filters this list by applying the supplied regular expression to each
     * column displayed in the table on the main window. This method returns a
     * new PartsList object containing only the matching Part objects.
     *
     * @param regex The regular expression to filter by.
     * @return A PartsList containing only the matching Part objects.
     */
    public PartsList<Part> filter(String regex) {
        if (regex.trim().equals("")) {
            return (PartsList<Part>) this;
        } else {
            PartsList<Part> newList = new PartsList<Part>();
            for (Object o : this) {
                if (o instanceof Part) {
                    Part p = (Part) o;
                    if (Regex.match(regex, Integer.toString(p.getPartId()))
                            || Regex.match(regex, p.getName())
                            || Regex.match(regex, Integer.toString(p.getInStock()))
                            || Regex.match(regex, Double.toString(p.getPrice()))) {
                        newList.add(p);
                    }
                }
            }
            return newList;
        }
    }

    /**
     * Returns an ID number not currently used in by any part in this PartsList
     * object. In a real implementation this would be supplied by the database's
     * auto-increment function of the primary key field and old ID's would never
     * be repeated.
     *
     * @return
     */
    public int getNewId() {
        int newId = 0;
        boolean found;
        do {
            found = false;
            for (Object o : this) {
                if (o instanceof Part) {
                    Part p = (Part) o;
                    if (p.getPartId() == newId) {
                        found = true;
                    }
                }
            }
            if (found) {
                newId++;
            }
        } while (found);
        return newId;
    }

    /**
     * Finds and returns the part identified by the supplied ID number, or
     * returns null if the part could not be found.
     *
     * @param id
     * @return
     */
    public Part getById(int id) {
        if (id < 0) {
            return null;
        } else {
            Part thePart = null;
            for (Object o : this) {
                if (o instanceof Part) {
                    Part p = (Part) o;
                    if (p.getPartId() == id) {
                        thePart = p;
                        break;
                    }
                }
            }
            return thePart;
        }
    }

    /**
     * Returns the sum of the prices of all Part objects in this PartsList.
     *
     * @return
     */
    public double getTotalCost() {
        double totalCost = 0;
        for (Object o : this) {
            if (o instanceof Part) {
                Part p = (Part) o;
                totalCost += p.getPrice();
            }
        }
        return totalCost;
    }

    /**
     * Returns the sum of the prices of all Part objects in the supplied
     * PartsList.
     *
     * @param list
     * @return
     */
    public static double getTotalCost(ObservableList list) {
        if (list instanceof PartsList) {
            PartsList pList = (PartsList) list;
            return pList.getTotalCost();
        }
        return 0;
    }
}
