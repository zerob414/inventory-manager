/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Convenience class to condense simple regex matching into a single line.
 *
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Regex {

    /**
     * Runs a regex match on the input using the pattern and returns a boolean
     * value indicating whether or not the pattern matched.
     *
     * @param pattern
     * @param input
     * @return
     */
    public static boolean match(String pattern, String input) {
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(input);
        return m.find();
    }
}
