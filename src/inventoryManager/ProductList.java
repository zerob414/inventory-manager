/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Custom list class to provide some extra helper/convenience methods for lists
 * of Product objects.
 *
 * @author Eric Boring
 * @param <E>
 */
public class ProductList<E> extends SimpleListProperty<E> {

    /**
     * Constructs a new ProductList with an empty observableArrayList.
     */
    public ProductList() {
        super(FXCollections.observableArrayList());
    }

    /**
     * Constructs a new ProductList with a supplied observableArrayList.
     *
     * @param initialValue
     */
    public ProductList(ObservableList<E> initialValue) {
        super(initialValue);
    }

    /**
     * Constructs a new ProductList with an empty observableArrayList and
     * supplied bean and name.
     *
     * @param bean
     * @param name
     */
    public ProductList(Object bean, String name) {
        super(bean, name, FXCollections.observableArrayList());
    }

    /**
     * Constructs a new ProductList with a supplied observableArrayList bean and
     * name.
     *
     * @param bean
     * @param name
     * @param initialValue
     */
    public ProductList(Object bean, String name, ObservableList<E> initialValue) {
        super(bean, name, initialValue);
    }

    /**
     * Filters this list by applying the supplied regular expression to each
     * column displayed in the table on the main window. This method returns a
     * new ProductList object containing only the matching Product objects.
     *
     * @param regex The regular expression to filter by.
     * @return A PartsList containing only the matching Part objects.
     */
    public ProductList<Product> filter(String regex) {
        if (regex.trim().equals("")) {
            return (ProductList<Product>) this;
        } else {
            ProductList<Product> newList = new ProductList<Product>();
            for (Object o : this) {
                if (o instanceof Product) {
                    Product p = (Product) o;
                    if (Regex.match(regex, Integer.toString(p.getProductId()))
                            || Regex.match(regex, p.getName())
                            || Regex.match(regex, Integer.toString(p.getInStock()))
                            || Regex.match(regex, Double.toString(p.getPrice()))) {
                        newList.add(p);
                    }
                }
            }
            return newList;
        }
    }

    /**
     * Returns an ID number not currently used in by any product in this
     * ProductList object. In a real implementation this would be supplied by
     * the database's auto-increment function of the primary key field and old
     * ID's would never be repeated.
     *
     * @return
     */
    public int getNewId() {
        int newId = 0;
        boolean found;
        do {
            found = false;
            for (Object o : this) {
                if (o instanceof Product) {
                    Product p = (Product) o;
                    if (p.getProductId() == newId) {
                        found = true;
                    }
                }
            }
            if (found) {
                newId++;
            }
        } while (found);
        return newId;
    }

    /**
     * Finds and returns the product identified by the supplied ID number, or
     * returns null if the product could not be found.
     *
     * @param id
     * @return
     */
    public Product getById(int id) {
        if (id < 0) {
            return null;
        } else {
            Product theProduct = null;
            for (Object o : this) {
                if (o instanceof Product) {
                    Product p = (Product) o;
                    if (p.getProductId() == id) {
                        theProduct = p;
                        break;
                    }
                }
            }
            return theProduct;
        }
    }
}
