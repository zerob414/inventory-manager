/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import javafx.collections.ObservableList;

/**
 * Object to represent a Product in the inventory.
 *
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Product {

    private PartsList<Part> associatedParts = new PartsList<>();
    private int productId;
    private String name;
    private double price;
    private int inStock;
    private int minimum;
    private int maximum;

    /**
     * Creates a new Product object and sets it's fields to the supplied values.
     *
     * @param productId
     * @param name
     * @param price
     * @param inStock
     * @param minimum
     * @param maximum
     * @param associatedParts
     */
    public Product(int productId, String name, double price, int inStock, int minimum, int maximum, PartsList associatedParts) {
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.minimum = minimum;
        this.maximum = maximum;
        this.associatedParts = associatedParts;
    }

    /**
     * Creates a new Product object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param productId
     * @param name
     * @param price
     * @param inStock
     * @param minimum
     * @param maximum
     */
    public Product(int productId, String name, double price, int inStock, int minimum, int maximum) {
        this(productId, name, price, inStock, minimum, 10, new PartsList());
    }

    /**
     * Creates a new Product object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param productId
     * @param name
     * @param price
     * @param inStock
     * @param minimum
     */
    public Product(int productId, String name, double price, int inStock, int minimum) {
        this(productId, name, price, inStock, minimum, 10, new PartsList());
    }

    /**
     * Creates a new Product object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param productId
     * @param name
     * @param price
     * @param inStock
     */
    public Product(int productId, String name, double price, int inStock) {
        this(productId, name, price, inStock, 0, 10, new PartsList());
    }

    /**
     * Creates a new Product object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param productId
     * @param name
     * @param price
     */
    public Product(int productId, String name, double price) {
        this(productId, name, price, 0, 0, 10, new PartsList());
    }

    /**
     * Creates a new Product object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param productId
     * @param name
     */
    public Product(int productId, String name) {
        this(productId, name, 0.0, 0, 0, 10, new PartsList());
    }

    /**
     * Creates a new Product object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param productId
     */
    public Product(int productId) {
        this(productId, "(Unamed)", 0.0, 0, 0, 10, new PartsList());
    }

    /**
     * Creates a new Product object and sets it's fields to defaults.
     */
    public Product() {
        this(0, "(Unamed)", 0.0, 0, 0, 10, new PartsList());
    }

    /**
     * Adds a Part to this Product's part list.
     *
     * @param p The Part object to add.
     */
    public void addAssociatedPart(Part p) {
        associatedParts.add(p);
    }

    /**
     * Removes a Part from this Product's part list.
     *
     * @param p The Part object to remove.
     * @return A boolean indicating whether the removal was successful.
     */
    public boolean removeAssociatedPart(Part p) {
        return associatedParts.remove(p);
    }

    /**
     * Removes a Part from this Product's part list based on the Part's ID
     * number.
     *
     * @param id The ID number of the Part object to remove.
     * @return A boolean indicating whether the removal was successful.
     */
    public boolean removeAssociatedPart(int id) {
        return removeAssociatedPart(lookupAssociatedPart(id));
    }

    /**
     * Returns the part identified by the supplied ID number, or null if the
     * part could not be found.
     *
     * @param id
     * @return
     */
    public Part lookupAssociatedPart(int id) {
        if (id < 0) {
            return null;
        } else {
            Part thePart = null;
            for (Object o : getAssociatedParts()) {
                if (o instanceof Part) {
                    Part p = (Part) o;
                    if (p.getPartId() == id) {
                        thePart = p;
                        break;
                    }
                }
            }
            return thePart;
        }
    }

    /**
     * Returns the PartsList of this Product.
     *
     * @return
     */
    public PartsList<Part> getAssociatedParts() {
        return associatedParts;
    }

    /**
     * Sets the PartsList of this Product.
     *
     * @param associatedParts
     */
    public void setAssociatedParts(PartsList<Part> associatedParts) {
        this.associatedParts = associatedParts;
    }

    /**
     * Returns this Product's ID number.
     *
     * @return
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets this Product's ID number.
     *
     * @param productId
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Returns this Product's name.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets this Product's name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns this Product's price.
     *
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set's this Product's price.
     *
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Returns the number of this Product that are in stock.
     *
     * @return
     */
    public int getInStock() {
        return inStock;
    }

    /**
     * Sets the number of this Product that are in stock.
     *
     * @param inStock
     */
    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    /**
     * Returns the minimum number of this product to keep in stock.
     *
     * @return
     */
    public int getMinimum() {
        return minimum;
    }

    /**
     * Sets the minimum number of this product to keep in stock.
     *
     * @param minimum
     */
    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    /**
     * Returns the maximum number of this product to keep in stock.
     *
     * @return
     */
    public int getMaximum() {
        return maximum;
    }

    /**
     * Sets the maximum number of this product to keep in stock.
     *
     * @param maximum
     */
    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }

    /**
     * Convenience method to set all fields values at once.
     *
     * @param productId
     * @param name
     * @param price
     * @param inStock
     * @param minimum
     * @param maximum
     * @param associatedParts
     */
    public void setAll(int productId, String name, double price, int inStock, int minimum, int maximum, PartsList associatedParts) {
        setProductId(productId);
        setName(name);
        setPrice(price);
        setInStock(inStock);
        setMinimum(minimum);
        setMaximum(maximum);
        setAssociatedParts(associatedParts);
    }

    /**
     * Returns the total cost of all of the parts associated with this Product.
     *
     * @return
     */
    public double getTotalPartsCost() {
        return associatedParts.getTotalCost();
    }

    /**
     * Returns the suggested price of this Product based on a 25% markup from
     * total cost of it's associated parts.
     *
     * @return
     */
    public double getSuggestedPrice() {
        return getTotalPartsCost() * 1.25;
    }

    /**
     * Returns the total cost of all of the parts associated with the supplied
     * Product list.
     *
     * @param list
     * @return
     */
    public static double getTotalPartsCost(ObservableList list) {
        return PartsList.getTotalCost(list);
    }

    /**
     * Returns the suggested price of the supplied Product list based on a 25%
     * markup from total cost of it's associated parts.
     *
     * @param list
     * @return
     */
    public static double getSuggestedPrice(ObservableList list) {
        return getTotalPartsCost(list) * 1.25;
    }
}
