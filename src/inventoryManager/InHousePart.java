/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

/**
 * Class representing a part built in house by the company.
 *
 * @author Eric Boring <eboring@wgu.edu>
 */
public class InHousePart extends Part {

    private int machineId = 0;

    /**
     * Creates a new part object and sets it's fields to the supplied values.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     * @param max
     * @param machineId
     */
    public InHousePart(int id, String name, int level, double cost, int min, int max, int machineId) {
        super(id, name, level, cost, min, max);
        this.machineId = machineId;
    }

    /**
     * Returns the ID number of the machine that manufactures this part.
     *
     * @return
     */
    public int getMachineId() {
        return machineId;
    }

    /**
     * Sets the ID number of the machine that manufactures this part.
     *
     * @param machineId
     */
    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }

    /**
     * Convenience method to set all fields values at once.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     * @param max
     * @param machineId
     */
    public void setAll(int id, String name, int level, double cost, int min, int max, int machineId) {
        super.setAll(id, name, level, cost, min, max);
        this.setMachineId(machineId);
    }

    /**
     * Returns the value of the auxiliary field, the machine ID for this
     * subclass.
     *
     * @return
     */
    @Override
    public String getAuxField() {
        return Integer.toString(this.getMachineId());
    }

}
