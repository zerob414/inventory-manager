/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

/**
 * Object to represent a part in the inventory.
 *
 * @author Eric Boring
 */
public abstract class Part {

    private int partId;
    private String name;
    private int inStock;
    private double price;
    private int minimum;
    private int maximum;

    /**
     * Creates a new part object and sets it's fields to the supplied values.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     * @param max
     */
    public Part(int id, String name, int level, double cost, int min, int max) {
        this.partId = id;
        this.name = name;
        this.inStock = level;
        this.price = cost;
        this.maximum = max;
        this.minimum = min;
    }

    /**
     * Creates a new part object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     */
    public Part(int id, String name, int level, double cost, int min) {
        this(id, name, level, cost, min, 1000);
    }

    /**
     * Creates a new part object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     */
    public Part(int id, String name, int level, double cost) {
        this(id, name, level, cost, 0, 1000);
    }

    /**
     * Creates a new part object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param id
     * @param name
     * @param level
     */
    public Part(int id, String name, int level) {
        this(id, name, level, 0.0f, 0, 1000);
    }

    /**
     * Creates a new part object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param id
     * @param name
     */
    public Part(int id, String name) {
        this(id, name, 0, 0.0f, 0, 1000);
    }

    /**
     * Creates a new part object and sets it's fields to the supplied values,
     * values not supplied are set to defaults.
     *
     * @param id
     */
    public Part(int id) {
        this(id, "(Unnamed)", 0, 0.0f, 0, 1000);
    }

    /**
     * Creates a new part object and sets it's fields to default values.
     */
    public Part() {
        this(0, "(Unnamed)", 0, 0.0f, 0, 1000);
    }

    /**
     * Returns this part's ID number.
     *
     * @return
     */
    public int getPartId() {
        return partId;
    }

    /**
     * Sets this part's ID number.
     *
     * @param partId
     */
    public void setPartId(int partId) {
        this.partId = partId;
    }

    /**
     * Returns this part's name.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets this part's name.
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the number of this part in stock.
     *
     * @return
     */
    public int getInStock() {
        return inStock;
    }

    /**
     * Sets the number of this part in stock.
     *
     * @param inStock
     */
    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    /**
     * Returns this part's price.
     *
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets this part's price.
     *
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Returns the minimum number of this part to keep in stock.
     *
     * @return
     */
    public int getMinimum() {
        return minimum;
    }

    /**
     * Sets the minimum number of this part to keep in stock.
     *
     * @param minimum
     */
    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    /**
     * Returns the maximum number of this part to keep in stock.
     *
     * @return
     */
    public int getMaximum() {
        return maximum;
    }

    /**
     * Sets the maximum number of this part to keep in stock.
     *
     * @param maximum
     */
    public void setMaximum(int maximum) {
        this.maximum = maximum;
    }

    /**
     * Convenience method to set all fields values at once.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     * @param max
     */
    public void setAll(int id, String name, int level, double cost, int min, int max) {
        setPartId(id);
        setName(name);
        setInStock(level);
        setPrice(cost);
        setMinimum(min);
        setMaximum(max);
    }

    /**
     * Returns the value of the auxiliary field.
     *
     * @return
     */
    public abstract String getAuxField();
}
