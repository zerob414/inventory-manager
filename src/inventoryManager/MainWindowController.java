/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * Main controller for the Inventory Management System project.
 *
 * @author Eric Boring
 */
public class MainWindowController implements Initializable {

    private Stage myStage;
    private Inventory inventory;
    private Stage partsEditStage;
    private EditPartController partsController;
    private Stage productsEditStage;
    private EditProductController productsController;

    @FXML
    private TableView tblParts;
    @FXML
    private TableColumn colPartId;
    @FXML
    private TableColumn colPartName;
    @FXML
    private TableColumn colPartInvLevel;
    @FXML
    private TableColumn colPartCost;
    @FXML
    private TextField txtSearchParts;
    @FXML
    private TableView tblProducts;
    @FXML
    private TableColumn colProdId;
    @FXML
    private TableColumn colProdName;
    @FXML
    private TableColumn colProdInvLevel;
    @FXML
    private TableColumn colProdCost;
    @FXML
    private TextField txtSearchProducts;

    /**
     * Constructs a new MainWindowController and loads the accompanying dialogs.
     */
    public MainWindowController() {
        try {
            FXMLLoader partEditorLoader = new FXMLLoader(getClass().getResource("EditPart.fxml"));
            partEditorLoader.load();
            Scene scene = new Scene(partEditorLoader.getRoot());
            partsEditStage = new Stage();
            partsEditStage.setScene(scene);
            partsEditStage.initModality(Modality.APPLICATION_MODAL);
            partsController = partEditorLoader.getController();
            partsController.setStage(partsEditStage);
            partsController.setParent(this);

            FXMLLoader productEditorLoader = new FXMLLoader(getClass().getResource("EditProduct.fxml"));
            productEditorLoader.load();
            Scene scene2 = new Scene(productEditorLoader.getRoot());
            productsEditStage = new Stage();
            productsEditStage.setScene(scene2);
            productsEditStage.initModality(Modality.APPLICATION_MODAL);
            productsController = productEditorLoader.getController();
            productsController.setStage(productsEditStage);
            productsController.setParent(this);
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handler for when the user presses the "Add" button for parts. Opens the
     * Edit Part dialog, passing null as the part to indicate creating a new
     * part.
     */
    @FXML
    public void actionAddPart() {
        partsController.setPart(null);
        partsEditStage.setTitle("Add New Part");
        partsEditStage.show();
    }

    /**
     * Handler for when the user presses the "Modify" button for parts. Opens
     * the Edit Part dialog, passing the part selected in the table for
     * modification.
     */
    @FXML
    public void actionModifyPart() {
        Object selObj = tblParts.getSelectionModel().getSelectedItem();
        if (selObj != null && selObj instanceof Part) {
            Part selPart = (Part) selObj;
            partsController.setPart(selPart);
            partsEditStage.setTitle("Modify Part");
            partsEditStage.show();
        }
    }

    /**
     * Handler for when the user presses the "Delete" button for parts. Shows a
     * confirmation dialog to the user.
     */
    @FXML
    public void actionDeletePart() {
        Object selObj = tblParts.getSelectionModel().getSelectedItem();
        if (selObj != null && selObj instanceof Part) {
            Part selPart = (Part) selObj;
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Delete Part?");
            alert.setHeaderText("Do you really want to delete this part?");
            alert.setContentText(selPart.getName());

            ButtonType btnYes = new ButtonType("Yes");
            ButtonType btnNo = new ButtonType("No");

            alert.getButtonTypes().setAll(btnYes, btnNo);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.initOwner(myStage);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == btnYes) {
                inventory.deletePart(selPart);
            }
        }
    }

    /**
     * Handler for when the user presses the "Search" button for parts. Filters
     * based on the text entered in the search text field. The user may also
     * enter a regular expression for advanced searching.
     */
    @FXML
    public void actionSearchParts() {
        PartsList filtered = inventory.getAllParts().filter(txtSearchParts.getText().trim());
        tblParts.setItems(filtered);
    }

    /**
     * Handler for when the user presses the "X" button for parts. Clears the
     * search text field and displays all parts in the table.
     */
    @FXML
    public void clearPartSearch() {
        txtSearchParts.setText("");
        actionSearchParts();
    }

    /**
     * Handler for when the user presses the "Exit" button.
     *
     * @param e
     */
    @FXML
    public void actionExit(ActionEvent e) {
        Node src = (Node) e.getSource();
        exitApp(src.getScene().getWindow());
    }

    /**
     * Handler for when the user presses the "Add" button for products. Opens
     * the Edit Product dialog and passes null as the product to indicate
     * creating a new product.
     */
    @FXML
    public void actionAddProduct() {
        productsController.setProduct(null);
        productsEditStage.setTitle("Add New Product");
        productsEditStage.show();
    }

    /**
     * Handler for when the user presses the "Modify" button for products. Opens
     * the Edit Product dialog and passes the selected product for modification.
     */
    @FXML
    public void actionModifyProduct() {
        Object selObj = tblProducts.getSelectionModel().getSelectedItem();
        if (selObj != null && selObj instanceof Product) {
            Product selProduct = (Product) selObj;
            productsController.setProduct(selProduct);
            productsEditStage.setTitle("Modify Product");
            productsEditStage.show();
        }
    }

    /**
     * Handler for when the user presses the "Delete" button for products. Shows
     * a confirmation dialog to the user.
     */
    @FXML
    public void actionDeleteProduct() {
        Object selObj = tblProducts.getSelectionModel().getSelectedItem();
        if (selObj != null && selObj instanceof Product) {
            Product selProduct = (Product) selObj;
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Delete Product?");
            alert.setHeaderText("Do you really want to delete this product?");
            alert.setContentText(selProduct.getName());

            ButtonType btnYes = new ButtonType("Yes");
            ButtonType btnNo = new ButtonType("No");

            alert.getButtonTypes().setAll(btnYes, btnNo);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.initOwner(myStage);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == btnYes) {
                inventory.removeProduct(selProduct);
            }
        }
    }

    /**
     * Handler for when the user presses the "Search" button for products.
     * Filters based on the text entered in the search text field. The user may
     * also enter a regular expression for advanced searching.
     */
    @FXML
    public void actionSearchProducts() {
        ProductList filtered = inventory.getAllProducts().filter(txtSearchProducts.getText().trim());
        tblProducts.setItems(filtered);
    }

    /**
     * Handler for when the user presses the "X" button for products. Clears the
     * search text field and displays all products in the table.
     */
    @FXML
    public void clearProductSearch() {
        txtSearchProducts.setText("");
        actionSearchProducts();
    }

    /**
     * Shows a confirmation dialog to ask the user if they want to exit the
     * application and exits if they select yes.
     *
     * @param mainWindow
     */
    private void exitApp(Window mainWindow) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Exit Inventory Management System?");
        alert.setHeaderText("Are you sure you want to exit the Inventory Management System?");
        //alert.setContentText("Choose your option.");

        ButtonType btnYes = new ButtonType("Yes");
        ButtonType btnNo = new ButtonType("No");

        alert.getButtonTypes().setAll(btnYes, btnNo);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(mainWindow);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btnYes) {
            Platform.exit();
        }
    }

    /**
     * Forces the parts and products tables to refresh their displayed content.
     */
    public void refreshTables() {
        tblParts.refresh();
        tblProducts.refresh();
    }

    /**
     * Creates a new Inventory object to hold all of the parts and products,
     * also binds the table columns to the proper fields of the Part and Product
     * objects.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inventory = new Inventory();
        colPartId.setCellValueFactory(new PropertyValueFactory<Part, Integer>("partId"));
        colPartName.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        colPartInvLevel.setCellValueFactory(new PropertyValueFactory<Part, Integer>("inStock"));
        colPartCost.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));
        tblParts.setItems(inventory.getAllParts());

        colProdId.setCellValueFactory(new PropertyValueFactory<Part, Integer>("productId"));
        colProdName.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        colProdInvLevel.setCellValueFactory(new PropertyValueFactory<Part, Integer>("inStock"));
        colProdCost.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));
        tblProducts.setItems(inventory.getAllProducts());
    }

    /**
     * Returns this controller's stage.
     *
     * @return
     */
    public Stage getStage() {
        return myStage;
    }

    /**
     * Sets this controller's stage and redirects the event generated by the
     * user clicking the close button on the top right of the window so that a
     * confirmation dialog can be shown.
     *
     * @param stage
     */
    public void setStage(Stage stage) {
        this.myStage = stage;
        this.getStage().setOnCloseRequest(e -> {
            exitApp(myStage.getScene().getWindow());
            e.consume();
        });
    }

    /**
     * Returns this controller's Inventory object.
     *
     * @return
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * Sets this controller's Inventory object.
     *
     * @param inventory
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

}
