/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

/**
 *
 * @author Eric Boring <eboring@wgu.edu>
 */
public class OutsourcedPart extends Part {

    private String companyName = "";

    /**
     * Creates a new part object and sets it's fields to the supplied values.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     * @param max
     * @param supplier
     */
    public OutsourcedPart(int id, String name, int level, double cost, int min, int max, String supplier) {
        super(id, name, level, cost, min, max);
        this.companyName = supplier;
    }

    /**
     * Returns the name of the company that supplies this part.
     *
     * @return
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the name of the company that supplies this part.
     *
     * @param companyName
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Convenience method to set all fields values at once.
     *
     * @param id
     * @param name
     * @param level
     * @param cost
     * @param min
     * @param max
     * @param supplier
     */
    public void setAll(int id, String name, int level, double cost, int min, int max, String supplier) {
        super.setAll(id, name, level, cost, min, max);
        this.setCompanyName(supplier);
    }

    /**
     * Returns the value of the auxiliary field, the supplier name for this
     * subclass.
     *
     * @return
     */
    @Override
    public String getAuxField() {
        return this.getCompanyName();
    }
}
