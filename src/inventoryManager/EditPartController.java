/*
 * Inventory Management System
 * Project for Western Governors University, C482 Software I.
 */
package inventoryManager;

import java.lang.reflect.Field;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class Controller for the Edit Part dialog.
 *
 * @author Eric Boring
 */
public class EditPartController implements Initializable {

    private Stage myStage;
    private MainWindowController parent;
    private Part part;

    @FXML
    private RadioButton rdoInHouse;
    @FXML
    private RadioButton rdoOutsourced;
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtName;
    @FXML
    private Spinner<Integer> spnInvLevel;
    @FXML
    private TextField txtCost;
    @FXML
    private Spinner<Integer> spnMin;
    @FXML
    private Spinner<Integer> spnMax;
    @FXML
    private TextField txtAuxField;
    @FXML
    private Label lblAuxField;

    /**
     * Constructs a new EditPartController.
     */
    public EditPartController() {
    }

    /**
     * Constructs a new EditPartController and sets the controller's stage.
     *
     * @param myStage
     */
    public EditPartController(Stage myStage) {
        this.myStage = myStage;
    }

    /**
     * Constructs a new EditPartController, sets the controller's stage and the
     * controller's parent.
     *
     * @param myStage
     * @param parent
     */
    public EditPartController(Stage myStage, MainWindowController parent) {
        this.myStage = myStage;
        this.parent = parent;
    }

    /**
     * Constructs a new EditPartController, sets the controller's stage, the
     * controller's parent and initial Part.
     *
     * @param myStage
     * @param parent
     * @param part
     */
    public EditPartController(Stage myStage, MainWindowController parent, Part part) {
        this.myStage = myStage;
        this.parent = parent;
        this.setPart(part);
    }

    /**
     * Handler for when the user presses the "Save" button. If the controller's
     * part is null, a new Part object will be created and added to the
     * inventory, otherwise the part will be modified.
     *
     * @param e
     */
    @FXML
    public void actionSave(ActionEvent e) {
        if (validateInput()) {
            if (this.part == null) {
                if (rdoInHouse.isSelected()) {
                    this.part = new InHousePart(Integer.parseInt(txtId.getText()), txtName.getText(), spnInvLevel.getValue(), Double.parseDouble(txtCost.getText()), spnMin.getValue(), spnMax.getValue(), Integer.parseInt(txtAuxField.getText()));
                } else {
                    this.part = new OutsourcedPart(Integer.parseInt(txtId.getText()), txtName.getText(), spnInvLevel.getValue(), Double.parseDouble(txtCost.getText()), spnMin.getValue(), spnMax.getValue(), txtAuxField.getText());
                }
                parent.getInventory().addPart(this.part);
            } else {
                if (this.part instanceof InHousePart) {
                    InHousePart ihp = (InHousePart) this.part;
                    ihp.setAll(Integer.parseInt(txtId.getText()), txtName.getText(), spnInvLevel.getValue(), Double.parseDouble(txtCost.getText()), spnMin.getValue(), spnMax.getValue(), Integer.parseInt(txtAuxField.getText()));
                } else {
                    OutsourcedPart osp = (OutsourcedPart) this.part;
                    osp.setAll(Integer.parseInt(txtId.getText()), txtName.getText(), spnInvLevel.getValue(), Double.parseDouble(txtCost.getText()), spnMin.getValue(), spnMax.getValue(), txtAuxField.getText());
                }
                parent.refreshTables();
            }
            myStage.hide();
            clearForm();
        }
    }

    /**
     * Handler for when the user presses the "Cancel" button. A confirmation
     * dialog box is displayed.
     *
     * @param e
     */
    @FXML
    public void actionCancel(ActionEvent e) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Inventory Management System - Part Editor");
        alert.setHeaderText("Do you want to discard your changes to this part?");
        //alert.setContentText("Choose your option.");

        ButtonType btnYes = new ButtonType("Yes");
        ButtonType btnNo = new ButtonType("No");

        alert.getButtonTypes().setAll(btnYes, btnNo);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(parent.getStage());

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == btnYes) {
            myStage.hide();
            clearForm();
        }

    }

    /**
     * Changes the label text for the auxiliary field based on the type of part
     * the user has selected with the radio buttons.
     *
     * @param e
     */
    @FXML
    public void toggleAuxText(ActionEvent e) {
        if (rdoInHouse.isSelected()) {
            lblAuxField.setText("Machine ID:");
            txtAuxField.setPromptText("123");
        } else {
            lblAuxField.setText("Supplier Name:");
            txtAuxField.setPromptText("Acme Inc.");
        }
    }

    /**
     * Validates all data entered by the user and displays an error dialog if
     * any data is invalid.
     *
     * @return Boolean value indicating whether validation succeeded.
     */
    private boolean validateInput() {
        boolean valid = true;
        String errorMessage = "";
        if (rdoInHouse.isSelected() == rdoOutsourced.isSelected()) {
            errorMessage += "\u2022 Select either 'In-House' or 'Outsourced' for the part source.\n";
            valid = false;
        }
        if (!Regex.match("^\\d+$", txtId.getText().trim())) {
            errorMessage += "\u2022 ID can only be digits.\n";
            valid = false;
        }
        if (txtName.getText().length() < 4) {
            errorMessage += "\u2022 Name must be descriptive.\n";
            valid = false;
        }
        if (txtName.getText().length() > 255) {
            errorMessage += "\u2022 Name cannot be longer than 255 characters.\n";
            valid = false;
        }
        if (Regex.match("^\\d+$", txtName.getText().trim())) {
            errorMessage += "\u2022 Name cannot be only digits.\n";
            valid = false;
        }
        if ((int) spnInvLevel.getValue() < (int) spnMin.getValue() || (int) spnInvLevel.getValue() > (int) spnMax.getValue()) {
            errorMessage += "\u2022 Inventory level must be between minimum and maximum inventory.\n";
            valid = false;
        }
        if (!Regex.match("^\\d+\\.?\\d*$", txtCost.getText().trim())) {
            errorMessage += "\u2022 Cost must be a valid positive number.\n";
            valid = false;
        }
        if ((int) spnMin.getValue() > (int) spnMax.getValue()) {
            errorMessage += "\u2022 Minimum inventory must be less or equal to maximum inventory.\n";
            valid = false;
        }
        if ((int) spnMin.getValue() < 0) {
            errorMessage += "\u2022 Minimum inventory must be greater than zero.\n";
            valid = false;
        }
        if ((int) spnMax.getValue() < 0) {
            errorMessage += "\u2022 Maximum inventory must be greater than zero.\n";
            valid = false;
        }
        if (rdoOutsourced.isSelected()) {
            if (txtAuxField.getText().length() < 4) {
                errorMessage += "\u2022 Supplier name must be descriptive.\n";
                valid = false;
            }
            if (txtAuxField.getText().length() > 255) {
                errorMessage += "\u2022 Supplier name cannot be longer than 255 characters.\n";
                valid = false;
            }
            if (Regex.match("^\\d+$", txtAuxField.getText().trim())) {
                errorMessage += "\u2022 Supplier name cannot be only digits.\n";
                valid = false;
            }
        } else {
            if (txtAuxField.getText().length() == 0) {
                errorMessage += "\u2022 Enter a machine ID.\n";
                valid = false;
            }
            if (txtAuxField.getText().length() > 255) {
                errorMessage += "\u2022 Machine ID cannot be longer than 255 characters.\n";
                valid = false;
            }
            if (!Regex.match("^\\d+$", txtAuxField.getText().trim())) {
                errorMessage += "\u2022 Machine ID must be only digits.\n";
                valid = false;
            }
        }

        if (!valid) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Invalid Values Entered");
            alert.setHeaderText("You have entered some invalid values, please fix the issues below to continue.");
            alert.setContentText(errorMessage);
            alert.showAndWait();
        }
        return valid;
    }

    /**
     * Initializes the spinners.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SpinnerValueFactory<Integer> svfInvLevel = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 1);
        spnInvLevel.setValueFactory(svfInvLevel);

        SpinnerValueFactory<Integer> svfMin = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 1);
        spnMin.setValueFactory(svfMin);

        SpinnerValueFactory<Integer> svfMax = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, 1);
        spnMax.setValueFactory(svfMax);

        /*
        In JavaFX, spinners do not commit typed in values to the ValueFactory
        until the user presses the "Enter" key. This is apparently deliberate
        behavior according to the documentation, although it goes against 
        expected behavior and is frustrating. The following code to solve this
        problem is borrowed from Sergio and Robert here:
        https://stackoverflow.com/questions/32340476/manually-typing-in-text-in-javafx-spinner-is-not-updating-the-value-unless-user
         */
        for (Field field : getClass().getDeclaredFields()) {
            try {
                Object obj = field.get(this);
                if (obj != null && obj instanceof Spinner) {
                    ((Spinner) obj).focusedProperty().addListener((observable, oldValue, newValue) -> {
                        if (!newValue) {
                            ((Spinner) obj).increment(0);
                        }
                    });
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns the stage associated with this controller.
     *
     * @return
     */
    public Stage getStage() {
        return myStage;
    }

    /**
     * Sets the stage associated with this controller.
     *
     * @param stage
     */
    public void setStage(Stage stage) {
        this.myStage = stage;
    }

    /**
     * Returns the parent controller for this controller.
     *
     * @return
     */
    public MainWindowController getParent() {
        return parent;
    }

    /**
     * Sets the parent controller for this controller.
     *
     * @param parent
     */
    public void setParent(MainWindowController parent) {
        this.parent = parent;
    }

    /**
     * Returns the part this controller is currently viewing/modifying.
     *
     * @return
     */
    public Part getPart() {
        return part;
    }

    /**
     * Sets the part this controller is currently viewing/modifying.
     * Additionally this method will populate all of the form's fields with the
     * Part's data or will clear the form if null is passed.
     *
     * @param part The part to view or modify.
     */
    public void setPart(Part part) {
        this.part = part;
        if (part == null) {
            clearForm();
        } else {
            rdoInHouse.setSelected(part instanceof InHousePart);
            rdoOutsourced.setSelected(part instanceof OutsourcedPart);
            rdoInHouse.setDisable(true);
            rdoOutsourced.setDisable(true);
            txtId.setText(Integer.toString(part.getPartId()));
            txtName.setText(part.getName());
            spnInvLevel.getValueFactory().setValue(part.getInStock());
            txtCost.setText(Double.toString(part.getPrice()));
            spnMax.getValueFactory().setValue(part.getMaximum());
            spnMin.getValueFactory().setValue(part.getMinimum());
            txtAuxField.setText(part.getAuxField());
            toggleAuxText(null);
        }
    }

    /**
     * Clears the form's fields and sets them to default values.
     */
    public void clearForm() {
        rdoInHouse.setSelected(true);
        rdoOutsourced.setSelected(false);
        rdoInHouse.setDisable(false);
        rdoOutsourced.setDisable(false);
        txtId.setText(Integer.toString(parent.getInventory().getAllParts().getNewId()));
        txtName.setText("");
        spnInvLevel.getValueFactory().setValue(0);
        txtCost.setText("0.00");
        spnMax.getValueFactory().setValue(10);
        spnMin.getValueFactory().setValue(0);
        txtAuxField.setText("");
        toggleAuxText(null);
    }
}
