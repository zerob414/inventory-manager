# Inventory Manager

This is my project for the Java Software Development 1 class at Western Governor's University.

## Requirements

School policies do not allow me to upload the rubric, scenario or specific project requirements here, but I will paraphrase as best I can to relay the information.

I was required to write a desktop application for tracking inventory in a small warehouse. This application was to be purely a front-end and no storage or data persistance of any kind was to be added. Here are some of the general requirements:
- GUIs must look nearly identical to the GUI mock-ups provided by the school.
- Must include all classes and functionality shown in the UML diagram provided by the school. Additional classes and functionality can be added as required.
- Include the ability to add, modify, delete and search for both parts and products.
- Products are made up of one or more parts. This must be reflected in data structures and on the "Edit Product" dialog.
- Instance variables may only be accessable via getters and setters (private).
- The following exception controls must be implemented:
  - Prevent invalid values being entered for minimum and maximum parts to be stocked.
  - Ensure products have at least one part associated with them.
  - Prompt the user to confirm delete of parts or products.
  - Ensure the price of a product is not less than the total cost of it's parts.


## Development

This project was developed over the course of about three months during my spare time. It was developed using NetBeans (the IDE the school requires). The interface is JavaFX written in FXML without external CSS files. Minimal inline CSS is used on occasion. This was purely a front-end product, so no data persistance or storage of any type in implemented. I used regular expression searching to allow advanced search funtionality on the user interface. Searching still functions as expected if the user simply enters a word instead of a regular expression. I created a custom list model for the JavaFX Table components which included functionality to filter the list by regex and also add up the total cost of all of the parts contained. I created a similar custom list model for products as well.

## Shortcomings

There are several shortcomings I have identified in the project, some due to constraints applied by the scenario or rubric and others due to the limited amount of time I had to develop the application. Here are the main ones:
- The ID numbers for parts and products get reused after parts/products are deleted and new ones are created. Duplicate ID numbers are never created. This shortcoming is the result of not having a database backend. In a real implementation, the ID numbers would be provided by the database table's _auto-increment_ feature on the _primary key_ field.
- The interface feels a bit clunky in some places. I took as much liberty as I was allowed to attempt to improve it, but I was constrained by the GUI mock-ups the school provided.